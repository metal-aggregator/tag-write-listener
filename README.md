Sample input:
```json
{
    "Records": [
        {
            "eventID": "d769c2717f5a7d25b55c0d4a0994f2cb",
            "eventName": "MODIFY",
            "eventVersion": "1.1",
            "eventSource": "aws:dynamodb",
            "awsRegion": "us-east-2",
            "dynamodb": {
                "ApproximateCreationDateTime": 1590330861,
                "Keys": {
                    "tag_name": {
                        "S": "Doom Metal"
                    }
                },
                "NewImage": {
                    "reviewIds": {
                        "L": [
                            {
                                "S": "caaae0a1-981a-400d-a143-ee1fceaa10c6"
                            }
                        ]
                    },
                    "tag_name": {
                        "S": "Doom Metal"
                    },
                    "type": {
                        "S": "UNKNOWN"
                    }
                },
                "OldImage": {
                    "reviewIds": {
                        "L": [
                            {
                                "S": "caaae0a1-981a-400d-a143-ee1fceaa10c6"
                            }
                        ]
                    },
                    "tag_name": {
                        "S": "Doom Metal"
                    },
                    "type": {
                        "S": "GENRE"
                    }
                },
                "SequenceNumber": "46722500000000001377331162",
                "SizeBytes": 172,
                "StreamViewType": "NEW_AND_OLD_IMAGES"
            },
            "eventSourceARN": "arn:aws:dynamodb:us-east-2:182002067151:table/Tags/stream/2020-05-24T14:05:55.504"
        }
    ]
}
```

Sample output:
```
Success
```

Environment variables:
* REVIEW_SERVICE_URL - Default to http://10.0.75.1:6661
* REVIEW_PROCESSING_SERVICE_URL - Default to http://10.0.75.1:6662

The above defaults are from my local machine. See the Docker NAT ip4v entry from running `ipconfig`