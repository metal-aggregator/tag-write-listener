package com.ryan.amg.tag.listener;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TagWriteListenerApplication {
    public static void main(String[] args) {
        SpringApplication.run(TagWriteListenerApplication.class, args);
    }
}
