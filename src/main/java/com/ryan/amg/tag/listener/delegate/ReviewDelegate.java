package com.ryan.amg.tag.listener.delegate;

import com.ryan.amg.tag.listener.config.ServiceConfigurationProperties;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@AllArgsConstructor
public class ReviewDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(ReviewDelegate.class);
    private static final String REVIEW_SERVICE_ADD_GENRE_URI = "/api/reviews/{reviewId}/genres/{genre}";
    private static final String REVIEW_SERVICE_ADD_RELATED_BAND_URI = "/api/reviews/{reviewId}/relatedBands/{relatedBand}";
    private static final String REVIEW_SERVICE_ADD_SCORE_URI = "/api/reviews/{reviewId}/score/{score}";

    private final RestTemplate restTemplate;
    private final ServiceConfigurationProperties serviceConfigurationProperties;

    public void addGenreToReview(String reviewId, String genre) {
        LOG.info("Attempting to add genre={} onto reviewId={}", genre, reviewId);
        String targetUri = serviceConfigurationProperties.getReviewService() + REVIEW_SERVICE_ADD_GENRE_URI;
        restTemplate.put(targetUri, null, reviewId, genre);
    }

    public void addRelatedBandToReview(String reviewId, String relatedBand) {
        LOG.info("Attempting to add relatedBand={} onto reviewId={}", relatedBand, reviewId);
        String targetUri = serviceConfigurationProperties.getReviewService() + REVIEW_SERVICE_ADD_RELATED_BAND_URI;
        restTemplate.put(targetUri, null, reviewId, relatedBand);
    }

    public void addScoreToReview(String reviewId, String score) {
        LOG.info("Attempting to add score={} onto reviewId={}", score, reviewId);
        String targetUri = serviceConfigurationProperties.getReviewService() + REVIEW_SERVICE_ADD_SCORE_URI;
        restTemplate.put(targetUri, null, reviewId, score);
    }

}
