package com.ryan.amg.tag.listener.delegate;

import com.ryan.amg.tag.listener.config.ServiceConfigurationProperties;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@AllArgsConstructor
public class ReviewProcessingDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(ReviewProcessingDelegate.class);
    private static final String REVIEW_PROCESSING_SERVICE_DELETE_TAG_URI = "/api/reviews/{reviewId}/processing/tags/{tag}";

    private final RestTemplate restTemplate;
    private final ServiceConfigurationProperties serviceConfigurationProperties;

    public void deleteTagFromReview(String reviewId, String tag) {
        LOG.info("Attempting to remove tag={} from reviewId={}", tag, reviewId);
        String targetUri = serviceConfigurationProperties.getReviewProcessingService() + REVIEW_PROCESSING_SERVICE_DELETE_TAG_URI;
        restTemplate.delete(targetUri, reviewId, tag);
    }

}
