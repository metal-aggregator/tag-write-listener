package com.ryan.amg.tag.listener.domain;

import com.amazonaws.services.dynamodbv2.model.Record;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class RecordsWrapper {

    private List<Record> records = new ArrayList<>();

    public List<Record> getRecords() {
        return records;
    }

    @JsonProperty("Records")
    public RecordsWrapper setRecords(List<Record> records) {
        this.records = records;
        return this;
    }

}
