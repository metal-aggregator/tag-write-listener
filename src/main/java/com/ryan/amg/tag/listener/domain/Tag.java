package com.ryan.amg.tag.listener.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Tag {
    private String tagName;
    private TagType type;
    private List<String> reviewIds = new ArrayList<>();
}
