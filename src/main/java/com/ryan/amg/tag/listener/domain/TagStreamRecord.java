package com.ryan.amg.tag.listener.domain;

import com.ryan.amg.tag.listener.domain.aws.EventType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TagStreamRecord {
    private EventType operation;
    private Tag beforeTag;
    private Tag afterTag;
}
