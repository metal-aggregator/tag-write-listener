package com.ryan.amg.tag.listener.domain;

public enum TagType {
    GENRE,
    BAND,
    SCORE,
    OTHER,
    UNKNOWN
}
