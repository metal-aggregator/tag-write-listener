package com.ryan.amg.tag.listener.domain.aws;

public enum EventType {
    MODIFY,
    INSERT,
    REMOVE
}
