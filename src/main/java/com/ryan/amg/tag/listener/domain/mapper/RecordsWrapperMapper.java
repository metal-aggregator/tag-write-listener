package com.ryan.amg.tag.listener.domain.mapper;

import com.amazonaws.util.IOUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ryan.amg.tag.listener.domain.RecordsWrapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;

@Component
@AllArgsConstructor
public class RecordsWrapperMapper {

    private final ObjectMapper caseInsensitiveObjectMapper;

    public RecordsWrapper map(InputStream inputStream) {
        try {
            return mapRecordsFromInputStream(inputStream);
        } catch (IOException exception) {
            throw new RuntimeException("Unable to parse input stream.", exception);
        }
    }

    private RecordsWrapper mapRecordsFromInputStream(InputStream inputStream) throws IOException {
        byte[] inputBytes = IOUtils.toByteArray(inputStream);
        return caseInsensitiveObjectMapper.readValue(inputBytes, RecordsWrapper.class);
    }

}
