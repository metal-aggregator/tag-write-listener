package com.ryan.amg.tag.listener.domain.mapper;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.Record;
import com.ryan.amg.tag.listener.domain.Tag;
import com.ryan.amg.tag.listener.domain.TagStreamRecord;
import com.ryan.amg.tag.listener.domain.TagType;
import com.ryan.amg.tag.listener.domain.aws.EventType;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class TagStreamRecordMapper {

    private static final String KEY_TAG_NAME = "tag_name";
    private static final String KEY_REVIEW_IDS = "reviewIds";
    private static final String KEY_TYPE = "type";

    public TagStreamRecord map(Record record) {
        EventType eventType = EventType.valueOf(record.getEventName());
        Tag beforeTag = mapTagFromSource(record.getDynamodb().getOldImage());
        Tag afterTag = mapTagFromSource(record.getDynamodb().getNewImage());
        return new TagStreamRecord(eventType, beforeTag, afterTag);
    }

    public Tag mapTagFromSource(Map<String, AttributeValue> sourceMap) {
        if (sourceMap == null) {
            return null;
        }
        String tagName = sourceMap.get(KEY_TAG_NAME).getS();
        TagType type = TagType.valueOf(sourceMap.get(KEY_TYPE).getS());
        List<String> reviewIds = sourceMap.get(KEY_REVIEW_IDS).getL().stream().map(AttributeValue::getS).collect(Collectors.toList());

        return new Tag(tagName, type, reviewIds);
    }

}
