package com.ryan.amg.tag.listener.function;

import com.google.gson.Gson;
import com.ryan.amg.tag.listener.domain.RecordsWrapper;
import com.ryan.amg.tag.listener.domain.mapper.RecordsWrapperMapper;
import com.ryan.amg.tag.listener.service.TagWriteListenerService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.function.Function;

@Component("com.ryan.amg.loader.handler.aws.TagWriteListenerHandler")
@AllArgsConstructor
public class TagWriteListenerFunction implements Function<InputStream, Object> {

    private static final Logger LOG = LoggerFactory.getLogger(TagWriteListenerFunction.class);

    private final RecordsWrapperMapper recordsWrapperMapper;
    private final TagWriteListenerService tagWriteListenerService;

    @Override
    public Object apply(InputStream inputStream) {
        RecordsWrapper recordsWrapper = recordsWrapperMapper.map(inputStream);
        LOG.debug("Incoming tag write event: " + new Gson().toJson(recordsWrapper));
        tagWriteListenerService.applyTagStreamRecords(recordsWrapper.getRecords());
        return "Success";
    }

}
