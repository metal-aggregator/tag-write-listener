package com.ryan.amg.tag.listener.processor;

import com.ryan.amg.tag.listener.delegate.ReviewDelegate;
import com.ryan.amg.tag.listener.domain.Tag;
import com.ryan.amg.tag.listener.domain.TagStreamRecord;
import com.ryan.amg.tag.listener.domain.TagType;
import com.ryan.amg.tag.listener.domain.aws.EventType;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class GenreResolvedRecordProcessor implements RecordProcessor {

    private final ReviewDelegate reviewDelegate;

    @Override
    public boolean supports(TagStreamRecord tagStreamRecord) {
        return isModifyOperation(tagStreamRecord) && tagTypeChangedToGenre(tagStreamRecord);
    }

    @Override
    public void process(TagStreamRecord tagStreamRecord) {
        String genre = tagStreamRecord.getAfterTag().getTagName();
        List<String> reviewIds = tagStreamRecord.getAfterTag().getReviewIds();
        reviewIds.forEach(k -> reviewDelegate.addGenreToReview(k, genre));
    }

    private boolean isModifyOperation(TagStreamRecord tagStreamRecord) {
        return tagStreamRecord.getOperation() == EventType.MODIFY;
    }

    private boolean tagTypeChangedToGenre(TagStreamRecord tagStreamRecord) {
        return isTagTypeUnknown(tagStreamRecord.getBeforeTag()) && isTagTypeGenre(tagStreamRecord.getAfterTag());
    }

    private boolean isTagTypeUnknown(Tag tag) {
        return (tag != null) && (tag.getType() == TagType.UNKNOWN);
    }

    private boolean isTagTypeGenre(Tag tag) {
        return (tag != null) && (tag.getType() == TagType.GENRE);
    }

}
