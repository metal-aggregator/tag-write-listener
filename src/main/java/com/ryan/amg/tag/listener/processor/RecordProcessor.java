package com.ryan.amg.tag.listener.processor;

import com.ryan.amg.tag.listener.domain.TagStreamRecord;

public interface RecordProcessor {
    boolean supports(TagStreamRecord tagStreamRecord);
    void process(TagStreamRecord tagStreamRecord);
}
