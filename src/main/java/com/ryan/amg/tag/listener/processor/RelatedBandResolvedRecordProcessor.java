package com.ryan.amg.tag.listener.processor;

import com.ryan.amg.tag.listener.delegate.ReviewDelegate;
import com.ryan.amg.tag.listener.domain.Tag;
import com.ryan.amg.tag.listener.domain.TagStreamRecord;
import com.ryan.amg.tag.listener.domain.TagType;
import com.ryan.amg.tag.listener.domain.aws.EventType;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class RelatedBandResolvedRecordProcessor implements RecordProcessor {

    private final ReviewDelegate reviewDelegate;

    @Override
    public boolean supports(TagStreamRecord tagStreamRecord) {
        return isModifyOperation(tagStreamRecord) && tagTypeChangedToBand(tagStreamRecord);
    }

    @Override
    public void process(TagStreamRecord tagStreamRecord) {
        String relatedBand = tagStreamRecord.getAfterTag().getTagName();
        List<String> reviewIds = tagStreamRecord.getAfterTag().getReviewIds();
        reviewIds.forEach(k -> reviewDelegate.addRelatedBandToReview(k, relatedBand));
    }

    private boolean isModifyOperation(TagStreamRecord tagStreamRecord) {
        return tagStreamRecord.getOperation() == EventType.MODIFY;
    }

    private boolean tagTypeChangedToBand(TagStreamRecord tagStreamRecord) {
        return isTagTypeUnknown(tagStreamRecord.getBeforeTag()) && isTagTypeBand(tagStreamRecord.getAfterTag());
    }

    private boolean isTagTypeUnknown(Tag tag) {
        return (tag != null) && (tag.getType() == TagType.UNKNOWN);
    }

    private boolean isTagTypeBand(Tag tag) {
        return (tag != null) && (tag.getType() == TagType.BAND);
    }

}
