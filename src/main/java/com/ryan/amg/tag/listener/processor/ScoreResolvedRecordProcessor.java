package com.ryan.amg.tag.listener.processor;

import com.ryan.amg.tag.listener.delegate.ReviewDelegate;
import com.ryan.amg.tag.listener.domain.Tag;
import com.ryan.amg.tag.listener.domain.TagStreamRecord;
import com.ryan.amg.tag.listener.domain.TagType;
import com.ryan.amg.tag.listener.domain.aws.EventType;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class ScoreResolvedRecordProcessor implements RecordProcessor {

    private final ReviewDelegate reviewDelegate;

    @Override
    public boolean supports(TagStreamRecord tagStreamRecord) {
        return isModifyOperation(tagStreamRecord) && tagTypeChangedToScore(tagStreamRecord);
    }

    @Override
    public void process(TagStreamRecord tagStreamRecord) {
        String score = tagStreamRecord.getAfterTag().getTagName();
        List<String> reviewIds = tagStreamRecord.getAfterTag().getReviewIds();
        reviewIds.forEach(k -> reviewDelegate.addScoreToReview(k, score));
    }

    private boolean isModifyOperation(TagStreamRecord tagStreamRecord) {
        return tagStreamRecord.getOperation() == EventType.MODIFY;
    }

    private boolean tagTypeChangedToScore(TagStreamRecord tagStreamRecord) {
        return isTagTypeUnknown(tagStreamRecord.getBeforeTag()) && isTagTypeScore(tagStreamRecord.getAfterTag());
    }

    private boolean isTagTypeUnknown(Tag tag) {
        return (tag != null) && (tag.getType() == TagType.UNKNOWN);
    }

    private boolean isTagTypeScore(Tag tag) {
        return (tag != null) && (tag.getType() == TagType.SCORE);
    }

}
