package com.ryan.amg.tag.listener.processor;

import com.ryan.amg.tag.listener.delegate.ReviewProcessingDelegate;
import com.ryan.amg.tag.listener.domain.Tag;
import com.ryan.amg.tag.listener.domain.TagStreamRecord;
import com.ryan.amg.tag.listener.domain.TagType;
import com.ryan.amg.tag.listener.domain.aws.EventType;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class TypeNoLongerUnknownRecordProcessor implements RecordProcessor {

    private final ReviewProcessingDelegate reviewProcessingDelegate;

    @Override
    public boolean supports(TagStreamRecord tagStreamRecord) {
        return isModifyOperation(tagStreamRecord) && tagTypeChangedFromUnknown(tagStreamRecord);
    }

    @Override
    public void process(TagStreamRecord tagStreamRecord) {
        String genre = tagStreamRecord.getAfterTag().getTagName();
        List<String> reviewIds = tagStreamRecord.getAfterTag().getReviewIds();
        reviewIds.forEach(k -> reviewProcessingDelegate.deleteTagFromReview(k, genre));
    }

    private boolean isModifyOperation(TagStreamRecord tagStreamRecord) {
        return tagStreamRecord.getOperation() == EventType.MODIFY;
    }

    private boolean tagTypeChangedFromUnknown(TagStreamRecord tagStreamRecord) {
        return isTagTypeUnknown(tagStreamRecord.getBeforeTag()) && isTagTypeResolved(tagStreamRecord.getAfterTag());
    }

    private boolean isTagTypeUnknown(Tag tag) {
        return (tag != null) && (tag.getType() == TagType.UNKNOWN);
    }

    private boolean isTagTypeResolved(Tag tag) {
        return (tag != null) && (tag.getType() != null) && (tag.getType() != TagType.UNKNOWN);
    }

}
