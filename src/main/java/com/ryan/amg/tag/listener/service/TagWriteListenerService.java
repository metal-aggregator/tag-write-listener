package com.ryan.amg.tag.listener.service;

import com.amazonaws.services.dynamodbv2.model.Record;
import com.google.gson.Gson;
import com.ryan.amg.tag.listener.domain.TagStreamRecord;
import com.ryan.amg.tag.listener.domain.mapper.TagStreamRecordMapper;
import com.ryan.amg.tag.listener.processor.RecordProcessor;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class TagWriteListenerService {

    private static final Logger LOG = LoggerFactory.getLogger(TagWriteListenerService.class);

    private final List<RecordProcessor> recordProcessors;
    private final TagStreamRecordMapper tagStreamRecordMapper;

    public void applyTagStreamRecords(List<Record> rawTagStreamRecords) {
        rawTagStreamRecords.stream().map(tagStreamRecordMapper::map).forEach(this::applyTagStreamRecord);
    }

    private void applyTagStreamRecord(TagStreamRecord tagStreamRecord) {
        LOG.info("Attempting to apply record processors to tagStreamRecord={}", new Gson().toJson(tagStreamRecord));
        recordProcessors.stream().filter(k -> k.supports(tagStreamRecord)).forEach(k -> k.process(tagStreamRecord));
    }

}
