package com.ryan.amg.tag.listener.delegate

import com.ryan.amg.tag.listener.config.ServiceConfigurationProperties
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

class ReviewDelegateSpec extends Specification {

    RestTemplate mockRestTemplate = Mock()
    ServiceConfigurationProperties mockServiceConfigurationProperties = Mock()

    ReviewDelegate reviewDelegate = new ReviewDelegate(mockRestTemplate, mockServiceConfigurationProperties)

    def "Calling addGenreToReview correctly invokes the RestTemplate to call the review-service's PUT genre endpoint"() {

        given:
            1 * mockServiceConfigurationProperties.getReviewService() >> 'http://reviewService.com'

        when:
            reviewDelegate.addGenreToReview('ReviewId', 'Genre')

        then:
            1 * mockRestTemplate.put('http://reviewService.com/api/reviews/{reviewId}/genres/{genre}', null, 'ReviewId', 'Genre')

    }

    def "Calling addRelatedBandToReview correctly invokes the RestTemplate to call the review-service's PUT relatedBand endpoint"() {

        given:
            1 * mockServiceConfigurationProperties.getReviewService() >> 'http://reviewService.com'

        when:
            reviewDelegate.addRelatedBandToReview('ReviewId', 'Anthrax')

        then:
            1 * mockRestTemplate.put('http://reviewService.com/api/reviews/{reviewId}/relatedBands/{relatedBand}', null, 'ReviewId', 'Anthrax')

    }

    def "Calling addScoreToReview correctly invokes the RestTemplate to call the review-service's PUT score endpoint"() {

        given:
            1 * mockServiceConfigurationProperties.getReviewService() >> 'http://reviewService.com'

        when:
            reviewDelegate.addScoreToReview('ReviewId', '4.5')

        then:
            1 * mockRestTemplate.put('http://reviewService.com/api/reviews/{reviewId}/score/{score}', null, 'ReviewId', '4.5')

    }

}
