package com.ryan.amg.tag.listener.delegate

import com.ryan.amg.tag.listener.config.ServiceConfigurationProperties
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

class ReviewProcessingDelegateSpec extends Specification {

    RestTemplate mockRestTemplate = Mock()
    ServiceConfigurationProperties mockServiceConfigurationProperties = Mock()

    ReviewProcessingDelegate reviewProcessingDelegate = new ReviewProcessingDelegate(mockRestTemplate, mockServiceConfigurationProperties)

    def "Calling deleteTagFromReview results in the RestTemplate's delete method getting correctly invoked"() {

        given:
            1 * mockServiceConfigurationProperties.getReviewProcessingService() >> 'http://mockReviewProcessingService'

        when:
            reviewProcessingDelegate.deleteTagFromReview('someReviewId', 'someTag')

        then:
            1 * mockRestTemplate.delete('http://mockReviewProcessingService/api/reviews/{reviewId}/processing/tags/{tag}', 'someReviewId', 'someTag')

    }

}
