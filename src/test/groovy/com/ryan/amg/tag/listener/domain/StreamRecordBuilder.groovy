package com.ryan.amg.tag.listener.domain

import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.amazonaws.services.dynamodbv2.model.Record
import com.amazonaws.services.dynamodbv2.model.StreamRecord

class StreamRecordBuilder {

    String eventName = ''
    String beforeTagName = ''
    List<String> beforeReviewIds = []
    String beforeType = ''
    String afterTagName = ''
    List<String> afterReviewIds = []
    String afterType = ''

    StreamRecordBuilder withEventName(String eventName) {
        this.eventName = eventName
        return this
    }

    StreamRecordBuilder withBeforeTagName(String beforeTagName) {
        this.beforeTagName = beforeTagName
        return this
    }

    StreamRecordBuilder withBeforeReviewIds(List<String> beforeReviewIds) {
        this.beforeReviewIds = beforeReviewIds
        return this
    }

    StreamRecordBuilder withBeforeType(String beforeType) {
        this.beforeType = beforeType
        return this
    }

    StreamRecordBuilder withAfterTagName(String afterTagName) {
        this.afterTagName = afterTagName
        return this
    }

    StreamRecordBuilder withAfterReviewIds(List<String> afterReviewIds) {
        this.afterReviewIds = afterReviewIds
        return this
    }

    StreamRecordBuilder withAfterType(String afterType) {
        this.afterType = afterType
        return this
    }

    Record build() {
        return new Record(
            eventName: eventName,
            dynamodb: buildStreamRecord()
        )
    }

    private StreamRecord buildStreamRecord() {
        return new StreamRecord(
            oldImage: [
                reviewIds: new AttributeValue().withL(beforeReviewIds.collect { new AttributeValue(it) }),
                tag_name: new AttributeValue(beforeTagName),
                type: new AttributeValue(beforeType)
            ],
            newImage: [
                reviewIds: new AttributeValue().withL(afterReviewIds.collect { new AttributeValue(it) }),
                tag_name: new AttributeValue(afterTagName),
                type: new AttributeValue(afterType)
            ]
        )
    }

}
