package com.ryan.amg.tag.listener.domain

class TagBuilder {
    String tagName = 'Amorphis'
    TagType type = TagType.BAND
    List<String> reviewIds = ['rev-y1']

    TagBuilder withTagName(String tagName) {
        this.tagName = tagName
        return this
    }

    TagBuilder withType(TagType type) {
        this.type = type
        return this
    }

    TagBuilder withReviewIds(List<String> reviewIds) {
        this.reviewIds = reviewIds
        return this
    }

    Tag build() {
        return new Tag(
            tagName: tagName,
            type: type,
            reviewIds: reviewIds
        )
    }

}
