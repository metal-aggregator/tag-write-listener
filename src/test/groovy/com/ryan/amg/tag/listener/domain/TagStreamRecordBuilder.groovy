package com.ryan.amg.tag.listener.domain

import com.ryan.amg.tag.listener.domain.aws.EventType

class TagStreamRecordBuilder {    
    
    EventType operation = EventType.INSERT
    Tag beforeTag = new TagBuilder().build()
    Tag afterTag = new TagBuilder().build()

    TagStreamRecordBuilder withOperation(EventType operation) {
        this.operation = operation
        return this
    }

    TagStreamRecordBuilder withBeforeTag(Tag beforeTag) {
        this.beforeTag = beforeTag
        return this
    }

    TagStreamRecordBuilder withAfterTag(Tag afterTag) {
        this.afterTag = afterTag
        return this
    }

    TagStreamRecord build() {
        return new TagStreamRecord(
            operation: operation,
            beforeTag: beforeTag,
            afterTag: afterTag
        )
    }
}
