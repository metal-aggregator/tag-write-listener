package com.ryan.amg.tag.listener.domain.mapper

import com.amazonaws.services.dynamodbv2.model.Record
import com.ryan.amg.tag.listener.domain.StreamRecordBuilder
import com.ryan.amg.tag.listener.domain.TagBuilder
import com.ryan.amg.tag.listener.domain.TagStreamRecord
import com.ryan.amg.tag.listener.domain.TagStreamRecordBuilder
import com.ryan.amg.tag.listener.domain.TagType
import com.ryan.amg.tag.listener.domain.aws.EventType
import spock.lang.Specification

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class TagStreamRecordMapperSpec extends Specification {

    TagStreamRecordMapper tagStreamRecordMapper = new TagStreamRecordMapper()

    def 'Invoking map() produces a well-formed TagStreamRecord object'() {

        given:
            Record inputRecord = new StreamRecordBuilder()
                    .withEventName('MODIFY')
                    .withBeforeType('UNKNOWN')
                    .withBeforeTagName('Death Metal')
                    .withBeforeReviewIds(['br1', 'br2'])
                    .withAfterType('BAND')
                    .withAfterTagName('Doom Metal')
                    .withAfterReviewIds(['ar1', 'ar2'])
                    .build()

            TagStreamRecord expectedTagStreamRecord = new TagStreamRecordBuilder()
                    .withOperation(EventType.MODIFY)
                    .withBeforeTag(new TagBuilder().withType(TagType.UNKNOWN).withTagName('Death Metal').withReviewIds(['br1', 'br2']).build())
                    .withAfterTag(new TagBuilder().withType(TagType.BAND).withTagName('Doom Metal').withReviewIds(['ar1', 'ar2']).build())
                    .build()

        when:
            TagStreamRecord actualTagStreamRecord = tagStreamRecordMapper.map(inputRecord)

        then:
            assertReflectionEquals(expectedTagStreamRecord, actualTagStreamRecord)

    }

    def 'Invoking map() produces a TagStreamRecord object with null before and after tags when the input has null old and new images'() {

        given:
            Record inputRecord = new StreamRecordBuilder().withEventName('MODIFY').build()
            inputRecord.getDynamodb().setOldImage(null)
            inputRecord.getDynamodb().setNewImage(null)

            TagStreamRecord expectedTagStreamRecord = new TagStreamRecordBuilder()
                    .withOperation(EventType.MODIFY)
                    .withBeforeTag(null)
                    .withAfterTag(null)
                    .build()

        when:
            TagStreamRecord actualTagStreamRecord = tagStreamRecordMapper.map(inputRecord)

        then:
            assertReflectionEquals(expectedTagStreamRecord, actualTagStreamRecord)

    }

}
