package com.ryan.amg.tag.listener.function

import com.amazonaws.services.dynamodbv2.model.Record
import com.ryan.amg.tag.listener.domain.RecordsWrapper
import com.ryan.amg.tag.listener.domain.StreamRecordBuilder
import com.ryan.amg.tag.listener.domain.mapper.RecordsWrapperMapper
import com.ryan.amg.tag.listener.service.TagWriteListenerService
import spock.lang.Specification

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class TagWriteListenerFunctionSpec extends Specification {

    TagWriteListenerService mockTagWriteListenerService = Mock()
    RecordsWrapperMapper mockRecordsWrapperMapper = Mock()

    TagWriteListenerFunction tagWriteListenerFunction = new TagWriteListenerFunction(mockRecordsWrapperMapper, mockTagWriteListenerService)

    def "Invoking apply() calls the service layer with the expected records from the mapped input wrapper"() {

        given:
            InputStream inputInputStream = new ByteArrayInputStream('Input data'.getBytes())

            InputStream expectedMapperInputStream = new ByteArrayInputStream('Input data'.getBytes())
            RecordsWrapper mockedRecordsWrapper = new RecordsWrapper(records: [new StreamRecordBuilder().build()])

            List<Record> expectedRecords = [new StreamRecordBuilder().build()]

            1 * mockRecordsWrapperMapper.map(_ as InputStream) >> { args ->
                assertReflectionEquals(expectedMapperInputStream, args.get(0))
                return mockedRecordsWrapper
            }

            1 * mockTagWriteListenerService.applyTagStreamRecords(_ as List<Record>) >> { args ->
                assertReflectionEquals(expectedRecords, (List<Record>) args.get(0))
            }

        when:
            String actualResult = tagWriteListenerFunction.apply(inputInputStream)

        then:
            actualResult == 'Success'

    }

}