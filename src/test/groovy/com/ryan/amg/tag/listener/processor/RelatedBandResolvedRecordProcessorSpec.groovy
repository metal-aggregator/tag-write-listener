package com.ryan.amg.tag.listener.processor

import com.ryan.amg.tag.listener.delegate.ReviewDelegate
import com.ryan.amg.tag.listener.domain.Tag
import com.ryan.amg.tag.listener.domain.TagBuilder
import com.ryan.amg.tag.listener.domain.TagStreamRecord
import com.ryan.amg.tag.listener.domain.TagStreamRecordBuilder
import com.ryan.amg.tag.listener.domain.TagType
import com.ryan.amg.tag.listener.domain.aws.EventType
import spock.lang.Specification
import spock.lang.Unroll

class RelatedBandResolvedRecordProcessorSpec extends Specification {

    ReviewDelegate mockReviewDelegate = Mock()

    RelatedBandResolvedRecordProcessor relatedBandResolvedRecordProcessor = new RelatedBandResolvedRecordProcessor(mockReviewDelegate)

    @Unroll
    def "Calling supports() returns #expectedResult when beforeTag=#beforeTagType and afterTag=#afterTagType"() {

        given:
            Tag beforeTag = new TagBuilder().withType(beforeTagType).build()
            Tag afterTag = new TagBuilder().withType(afterTagType).build()
            TagStreamRecord inputTagStreamRecord = new TagStreamRecordBuilder()
                    .withBeforeTag(beforeTag)
                    .withAfterTag(afterTag)
                    .withOperation(EventType.MODIFY)
                    .build()

        when:
            boolean actualResult = relatedBandResolvedRecordProcessor.supports(inputTagStreamRecord)

        then:
            expectedResult == actualResult

        where:
            beforeTagType   | afterTagType    | expectedResult
            TagType.BAND    | TagType.BAND    | false
            TagType.BAND    | TagType.SCORE   | false
            TagType.BAND    | TagType.GENRE   | false
            TagType.BAND    | TagType.OTHER   | false
            TagType.BAND    | TagType.UNKNOWN | false
            TagType.BAND    | null            | false
            TagType.SCORE   | TagType.BAND    | false
            TagType.SCORE   | TagType.SCORE   | false
            TagType.SCORE   | TagType.GENRE   | false
            TagType.SCORE   | TagType.OTHER   | false
            TagType.SCORE   | TagType.UNKNOWN | false
            TagType.SCORE   | null            | false
            TagType.GENRE   | TagType.BAND    | false
            TagType.GENRE   | TagType.SCORE   | false
            TagType.GENRE   | TagType.GENRE   | false
            TagType.GENRE   | TagType.OTHER   | false
            TagType.GENRE   | TagType.UNKNOWN | false
            TagType.GENRE   | null            | false
            TagType.OTHER   | TagType.BAND    | false
            TagType.OTHER   | TagType.SCORE   | false
            TagType.OTHER   | TagType.GENRE   | false
            TagType.OTHER   | TagType.OTHER   | false
            TagType.OTHER   | TagType.UNKNOWN | false
            TagType.OTHER   | null            | false
            TagType.UNKNOWN | TagType.BAND    | true
            TagType.UNKNOWN | TagType.SCORE   | false
            TagType.UNKNOWN | TagType.GENRE   | false
            TagType.UNKNOWN | TagType.OTHER   | false
            TagType.UNKNOWN | TagType.UNKNOWN | false
            TagType.UNKNOWN | null            | false
            null            | TagType.BAND    | false
            null            | TagType.SCORE   | false
            null            | TagType.GENRE   | false
            null            | TagType.OTHER   | false
            null            | TagType.UNKNOWN | false
            null            | null            | false

    }

    @Unroll
    def "Calling supports() returns #expectedResult when eventType=#eventType"() {

        given:
            Tag beforeTag = new TagBuilder().withType(TagType.UNKNOWN).build()
            Tag afterTag = new TagBuilder().withType(TagType.BAND).build()
            TagStreamRecord inputTagStreamRecord = new TagStreamRecordBuilder()
                .withBeforeTag(beforeTag)
                .withAfterTag(afterTag)
                .withOperation(eventType)
                .build()

        when:
            boolean actualResult = relatedBandResolvedRecordProcessor.supports(inputTagStreamRecord)

        then:
            expectedResult == actualResult

        where:
            eventType        | expectedResult
            EventType.MODIFY | true
            EventType.INSERT | false
            EventType.REMOVE | false

    }

    def "Calling process invokes the ReviewDelegate for every review id in the after image"() {

        given:
            Tag afterImageTag = new TagBuilder().withTagName('Anthrax').withReviewIds(['ReviewId1', 'ReviewId2']).build()
            TagStreamRecord inputTagStreamRecord = new TagStreamRecordBuilder().withAfterTag(afterImageTag).build()

            1 * mockReviewDelegate.addRelatedBandToReview('ReviewId1', 'Anthrax')
            1 * mockReviewDelegate.addRelatedBandToReview('ReviewId2', 'Anthrax')

        when:
            relatedBandResolvedRecordProcessor.process(inputTagStreamRecord)

        then:
            noExceptionThrown()

    }

}
