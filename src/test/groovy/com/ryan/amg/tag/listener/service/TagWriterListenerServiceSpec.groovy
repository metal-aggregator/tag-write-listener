package com.ryan.amg.tag.listener.service

import com.amazonaws.services.dynamodbv2.model.Record
import com.ryan.amg.tag.listener.domain.StreamRecordBuilder
import com.ryan.amg.tag.listener.domain.TagStreamRecord
import com.ryan.amg.tag.listener.domain.TagStreamRecordBuilder
import com.ryan.amg.tag.listener.domain.mapper.TagStreamRecordMapper
import com.ryan.amg.tag.listener.processor.RecordProcessor
import spock.lang.Specification
import spock.lang.Unroll

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class TagWriterListenerServiceSpec extends Specification {

    RecordProcessor mockRecordProcessor1 = Mock()
    RecordProcessor mockRecordProcessor2 = Mock()
    TagStreamRecordMapper mockTagStreamRecordMapper = Mock()

    TagWriteListenerService tagWriteListenerService = new TagWriteListenerService([mockRecordProcessor1, mockRecordProcessor2], mockTagStreamRecordMapper)

    @Unroll
    def "Invoking applyTagStreamRecords maps the input Records and processes them via the supporting RecordProcessors when #description"() {

        given:
            Record inputRecord = new StreamRecordBuilder().withBeforeReviewIds(['Review1']).withBeforeTagName('Death Metal').withBeforeType('UNKNOWN').withAfterReviewIds(['Review1']).withAfterTagName('Death Metal').withAfterType('GENRE').build()
            Record expectedRecord = new StreamRecordBuilder().withBeforeReviewIds(['Review1']).withBeforeTagName('Death Metal').withBeforeType('UNKNOWN').withAfterReviewIds(['Review1']).withAfterTagName('Death Metal').withAfterType('GENRE').build()

            TagStreamRecord mockedTagStreamRecord = new TagStreamRecordBuilder().build()
            TagStreamRecord expectedTagStreamRecord = new TagStreamRecordBuilder().build()

            1 * mockTagStreamRecordMapper.map(_ as Record) >> { args ->
                assertReflectionEquals(expectedRecord, (Record) args.get(0))
                return mockedTagStreamRecord
            }

            1 * mockRecordProcessor1.supports(_ as TagStreamRecord) >> { args ->
                assertReflectionEquals(expectedTagStreamRecord, (TagStreamRecord) args.get(0))
                return doesFirstProcessorSupport
            }

            1 * mockRecordProcessor2.supports(_ as TagStreamRecord) >> { args ->
                assertReflectionEquals(expectedTagStreamRecord, (TagStreamRecord) args.get(0))
                return doesSecondProcessorSupport
            }

            expectedFirstProcessorCount * mockRecordProcessor1.process(_ as TagStreamRecord) >> { args ->
                assertReflectionEquals(expectedTagStreamRecord, (TagStreamRecord) args.get(0))
            }

            expectedSecondProcessorCount * mockRecordProcessor2.process(_ as TagStreamRecord) >> { args ->
                assertReflectionEquals(expectedTagStreamRecord, (TagStreamRecord) args.get(0))
            }

        when:
            tagWriteListenerService.applyTagStreamRecords([inputRecord])

        then:
            noExceptionThrown()

        where:
            description                                      | doesFirstProcessorSupport | doesSecondProcessorSupport | expectedFirstProcessorCount | expectedSecondProcessorCount
            'neither RecordProcessor supports the Record'    | false                     | false                      | 0                           | 0
            'the first RecordProcessor supports the record'  | true                      | false                      | 1                           | 0
            'the second RecordProcessor supports the record' | false                     | true                       | 0                           | 1
            'both RecordProcessors supports the record'      | true                      | true                       | 1                           | 1

    }

    def "Invoking applyTagStreamRecords with multiple records invokes the RecordProcessors for each Record"() {

        given:
            Record inputRecord1 = new StreamRecordBuilder().build()
            Record inputRecord2 = new StreamRecordBuilder().build()

            TagStreamRecord tagStreamRecord1 = new TagStreamRecordBuilder().build()
            TagStreamRecord tagStreamRecord2 = new TagStreamRecordBuilder().build()

            2 * mockTagStreamRecordMapper.map(_ as Record) >>> [tagStreamRecord1, tagStreamRecord2]

            2 * mockRecordProcessor1.supports(_ as TagStreamRecord) >> true
            2 * mockRecordProcessor2.supports(_ as TagStreamRecord) >> true
            2 * mockRecordProcessor1.process(_ as TagStreamRecord)
            2 * mockRecordProcessor2.process(_ as TagStreamRecord)

        when:
            tagWriteListenerService.applyTagStreamRecords([inputRecord1, inputRecord2])

        then:
            noExceptionThrown()

    }

}
